import axios from 'axios';

const axiosPost = axios.create({
    baseURL: 'https://myblog-9b714.firebaseio.com/'
});

export default axiosPost;