import React, {useState} from 'react';
import './add.css';
import axiosPost from "../../axiosPosts";
import Spinner from "../../components/spinner/spinner";

const Add = props => {
    const [posts, setPosts] = useState({
        title: '',
        description: '',
    });

    const [loading, setLoading] = useState(false)

    const postDataChanger = event => {
        const name = event.target.name;
        const value = event.target.value;

        setPosts(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const postHandler = async event => {
        event.preventDefault();
        setLoading(true);

        const newPosts = {
            posts: {...posts},
        };

        try {
            await   axiosPost.post('/posts.json', newPosts);
        } finally {
            setLoading(false);
            props.history.push('/');
        }
    };

    if (loading) {
        return (
            <Spinner/>
        )
    } else {
        return (
            <div className="container">
                <div className="addPost">
                    <h2>Add new post</h2>
                    <form onSubmit={postHandler}>
                        <input value={posts.title} type="text" name="title" className="newPostTitle" placeholder="Title" onChange={postDataChanger}/>
                        <textarea value={posts.description} className="newPostDescription" name="description" placeholder="Description" onChange={postDataChanger}/>
                        <button type="submit" className="saveBtn">Save</button>
                    </form>
                </div>
            </div>
        );
    }
};

export default Add;