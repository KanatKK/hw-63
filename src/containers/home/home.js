import  React, {useEffect, useState} from 'react';
import './home.css';
import axiosPost from "../../axiosPosts";
import Spinner from "../../components/spinner/spinner";

const Home = props => {
    const [newPosts, setNewPosts] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const posts = await axiosPost.get('/posts.json');
            let newPostsCopy = [...newPosts]
            for (const key in posts.data) {
                if (typeof posts.data[key].posts.title === "string") {
                    newPostsCopy.push({
                        title: posts.data[key].posts.title,
                        id: key,
                    });
                }
            }
            setNewPosts(newPostsCopy);
        };
        fetchData().catch(e => console.log(e));
    }, []);

    let sendParams = event => {
        const params = new URLSearchParams({key: event.target.id});
        props.history.push({
            pathname: '/post',
            search: '?' + params.toString()
        });
    };

    let postsList = newPosts.map((post) => {
        if (newPosts.length !== 0) {
            return (
                <div className="post" key={post.id}>
                    <p className="postTitle">{post.title}</p>
                    <button type="button" className="postBtn" id={post.id} onClick={sendParams}>Read more >></button>
                </div>
            );
        }
    });

    if (newPosts.length === 0) {
        return (<Spinner/>)
    } else {
        return (
            <div className="container">
                {postsList}
            </div>
        );
    }
};

export default Home;