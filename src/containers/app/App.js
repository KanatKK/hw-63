import React from 'react';
import './App.css'
import Home from "../home/home";
import Add from "../add/add";
import Contacts from "../../components/contacts/contacts";
import AboutUs from "../../components/aboutUs/aboutUs";
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Header from "../../components/header/header";
import Post from "../../components/post/post";
import Edit from "../../components/edit/edit";

const App = () => {
    return (
        <BrowserRouter>
            <Route component={Header}/>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/aboutUs" exact component={AboutUs}/>
                <Route path="/add" exact component={Add}/>
                <Route path="/contacts" exact component={Contacts}/>
                <Route path="/post" exact component={Post}/>
                <Route path="/post/edit" component={Edit}/>
            </Switch>
        </BrowserRouter>
    );
};

export default App;