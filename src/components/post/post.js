import React, {useEffect, useState} from 'react';
import './post.css';
import axiosPost from "../../axiosPosts";
import Spinner from "../spinner/spinner";

const Post = props => {
    const [post, setPost] = useState([]);
    const getParams = () => {
        const params = new URLSearchParams(props.location.search);
        return Object.fromEntries(params);
    };

    const [keys] = useState(getParams);

    let postCopy = [...post];
    useEffect(() => {
        const fetchData = async () => {
            const postInner = await axiosPost.get('/posts/'+ keys.key +'.json');
            for (const key in postInner.data) {
                    postCopy[0] = {title: postInner.data[key].title, description: postInner.data[key].description,}
            }
            setPost(postCopy);
        };
        fetchData().catch(e => console.log(e));
    }, []);

    const [deletedPost] = useState({
        title: '',
    });

    const deletePost = async () => {

        try {
            await axiosPost.post('/posts/'+ keys.key +'/posts/title.json', deletedPost);
        } finally {
            props.history.push('/');
        }
    };

    let sendParams = event => {
        const params = new URLSearchParams({key: event.target.id});
        props.history.push({
            pathname: '/post/edit',
            search: '?' + params.toString()
        });
    };

    if (postCopy[0] !== undefined) {
            return (
                <div className="container">
                    <div className="postInner">
                        <button type="button" className="delete" onClick={deletePost}>X</button>
                        <h4 className="postHeading">{postCopy[0].title}</h4>
                        <p className="postDescription">{postCopy[0].description}</p>
                        <button type="button" className="edit" id={keys.key} onClick={sendParams}>Edit</button>
                    </div>
                </div>
            );
    } else {
        return (<Spinner/>);
    }
};

export default Post;