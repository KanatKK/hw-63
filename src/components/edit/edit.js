import React, {useEffect, useState} from 'react';
import './edit.css'
import axiosPost from "../../axiosPosts";
import Spinner from "../spinner/spinner";

const Edit = props => {
    const [editPost, setEditPost] = useState([]);
    const [editedPost, setEditedPost] = useState({
        title: '',
        description: ''
    });
    const getParams = () => {
        const params = new URLSearchParams(props.location.search);
        return Object.fromEntries(params);
    };

    const [keys] = useState(getParams);

    let postCopy = [...editPost];
    useEffect(() => {
        const fetchData = async () => {
            const postInner = await axiosPost.get('/posts/'+ keys.key +'.json');
            for (const key in postInner.data) {
                postCopy[0] = {title: postInner.data[key].title, description: postInner.data[key].description,}
            }
            setEditPost(postCopy);
        };
        fetchData().catch(e => console.log(e));
    }, []);

    const postDataChanger = event => {
        const name = event.target.name;
        const value = event.target.value;

        setEditedPost(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const edited = {
        posts: {...editedPost},
    };

    const sendEditedPost = async () => {
        try {
            await axiosPost.post('/posts/'+ keys.key +'/posts/title.json', editedPost);
            await axiosPost.post('/posts.json', edited);
        } finally {
            props.history.push('/');
        }
    };

    if (postCopy[0] !== undefined) {
        return (
            <div className="container">
                <div className="editPost">
                    <h2>Edit post</h2>
                    <form onSubmit={sendEditedPost}>
                        <input
                            defaultValue={postCopy[0].title}  type="text"
                            name="title" className="newPostTitle" placeholder="Title"
                            onChange={postDataChanger}
                        />
                        <textarea
                            defaultValue={postCopy[0].description}
                            className="newPostDescription" name="description"
                            placeholder="Description" onChange={postDataChanger}
                        />
                        <button type="submit" className="editBtn">Edit</button>
                    </form>
                </div>
            </div>
        );
    } else {
        return (<Spinner/>)
    }
};

export default Edit;