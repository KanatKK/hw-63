import React from 'react';
import './header.css'

const Header = props => {
    const onHomeClick = () => {
        props.history.push({
            pathname: '/'
        });
    };

    const onAddClick = () => {
        props.history.push({
            pathname: '/add'
        });
    };

    const onAboutUsClick = () => {
        props.history.push({
            pathname: '/aboutUs'
        });
    };

    const onContactsClick = () => {
        props.history.push({
            pathname: '/contacts'
        });
    };

    return (
        <div className="container">
            <header className="header">
                <h3 className="headerHeading">My Blog</h3>
                <div className="links">
                    <button type="button" onClick={onHomeClick} className="link" style={{borderRight: '2px solid black'}}>Home</button>
                    <button type="button" onClick={onAddClick} className="link" style={{borderRight: '2px solid black'}}>Add</button>
                    <button type="button" onClick={onAboutUsClick} className="link" style={{borderRight: '2px solid black'}}>About</button>
                    <button type="button" onClick={onContactsClick} className="link">Contacts</button>
                </div>
            </header>
        </div>
    )
};

export default Header;